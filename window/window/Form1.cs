﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        DataTable dtMain = new DataTable();
        private void Form1_Load(object sender, EventArgs e)
        {
          
            dtMain.Columns.Add("col1");
            dtMain.Columns.Add("col2");
            using (var reader = new StreamReader("danhsach.txt"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    DataRow rowadd = dtMain.NewRow();
                    rowadd["col1"] = line.Split(';')[0];
                    rowadd["col2"] = line.Split(';')[1];
                    dtMain.Rows.Add(rowadd);

                    // Do stuff with your line here, it will be called for each 
                    // line of text in your file.
                }
            }
            dataGridView1.DataSource = dtMain;

            for (int i = 0; i < dtMain.Rows.Count; i++)
            {
                int iplus = i + 1;
                DataRow dtG = dtMain.Rows[i];
                var timer = new NamedTimer(iplus.ToString(), dtG[0].ToString());
               
                int phut = int.Parse(dtG[1].ToString());
                timer.Interval = 60 * phut * 1000;
                timer.Elapsed += Main_Tick;
                timer.AutoReset = true;
                timer.Start();
            }

        }
        public class NamedTimer : System.Timers.Timer
        {
            public readonly string name;
            public readonly string url;
            public NamedTimer(string name,string url1)
            {
                this.name = name;
                this.url = url1;
            }
        }
         void Main_Tick(object sender, EventArgs args)
        {
            NamedTimer timer = sender as NamedTimer;
            Console.WriteLine(timer.name);
            try
            {
                //login
                var client = new WebClient();
                var text = client.DownloadString(timer.url);

                label1.BeginInvoke(new MethodInvoker(delegate { label1.Text = timer.url; }));
            }
            catch (Exception s)
            {
                label1.BeginInvoke(new MethodInvoker(delegate { label1.Text = s.ToString()+"Trade" + urlText; }));
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            bool isError = false;
            for (int i = 0; i < dtMain.Rows.Count; i++)
            {
                int iplus = i + 1;
                DataRow dtG = dtMain.Rows[i];
                try
                {
                    //login
                    var client = new WebClient();
                     client.DownloadString( dtG[0].ToString());

                    label1.BeginInvoke(new MethodInvoker(delegate { label1.Text =  dtG[0].ToString(); }));
                }
                catch (Exception s)
                {
                    isError = true;
                    label1.BeginInvoke(new MethodInvoker(delegate { label1.Text = s.ToString() + "Trade" + dtG[0].ToString(); }));
                }
            }
            if (isError == false)
            {
                label1.BeginInvoke(new MethodInvoker(delegate { label1.Text = "Xong"; })); 
            }

           
          
        }

        private void button2_Click(object sender, EventArgs e)
        {


        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                //login
                var client = new WebClient();
                var text = client.DownloadString(urlText);
                label1.Text = DateTime.Now.ToString("HH:mm:ss         yyyy.MM.dd ");
            }
            catch (Exception s)
            {
                label1.Text = s.ToString(); ;
            }
        }
        String urlText = "";
        private void button2_Click_1(object sender, EventArgs e)
        {
            urlText = textBox2Url.Text;
            //button cap nhat
            timer1.Interval = int.Parse(textBox1.Text) * 1000 * 60;
        }



    }
}
